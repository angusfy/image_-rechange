# Image_Rechange

#### 介绍
批量缩小图片

#### 软件架构
软件架构说明
QT5.3 +VS2013编译

#### 安装教程

1.  release版本在win32文件夹下的release文件夹里，已经使用qt的发行工具做了发行，但是完全没有编程环境的电脑可能还是会缺少组件，请自行处理。
2.详细使用教程在csdn有写：https://blog.csdn.net/angusfy/article/details/134292548?spm=1001.2014.3001.5502

#### 使用说明

1.  可选择多个文件一起压缩，也可选择整个文件夹，对文件夹内所有图片文件进行压缩，并且可以选择是否需要保持文件夹里子文件夹的目录。
2.  使用QT自带Qimage类函数进行压缩，支持压缩DPI和指定分辨率，不过速度感人。
3.  

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
