#ifndef IMAGE_RECHANGE_H
#define IMAGE_RECHANGE_H

#include <QtWidgets/QMainWindow>
#include "ui_image_rechange.h"
#include<QDragEnterEvent>
#include<QMimeData>
#include<QDropEvent>
#include<QUrl>
#include "QMessageBox"
#include "QFileDialog"
#include <QDir>
#include <QFileInfoList>
#include <QDirIterator>

class Image_Rechange : public QMainWindow
{
	Q_OBJECT

public:
	Image_Rechange(QWidget *parent = 0);
	~Image_Rechange();
	QStringList fileNames;
	QString srcDirPathChange;
	int numSum,nowSum;
	void startProgress(int nowSum);
	bool flag_doc;
private:
	Ui::Image_RechangeClass ui;

	private slots:
	void on_pushButton_openDoc_clicked();
	void on_pushButton_openDocs_clicked();
	void on_pushButton_changeDPI_clicked();
	void on_pushButton_changeFBL_clicked();
	void on_pushButton_clear_clicked();
	void on_checkbox_clicked();
	void on_checkbox_quality_clicked();
	void on_checkbox_speed_clicked();
};

#endif // IMAGE_RECHANGE_H
