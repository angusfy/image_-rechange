/****************************************************************************
** Meta object code from reading C++ file 'image_rechange.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.3.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../image_rechange.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'image_rechange.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.3.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Image_Rechange_t {
    QByteArrayData data[10];
    char stringdata[243];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Image_Rechange_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Image_Rechange_t qt_meta_stringdata_Image_Rechange = {
    {
QT_MOC_LITERAL(0, 0, 14),
QT_MOC_LITERAL(1, 15, 29),
QT_MOC_LITERAL(2, 45, 0),
QT_MOC_LITERAL(3, 46, 30),
QT_MOC_LITERAL(4, 77, 31),
QT_MOC_LITERAL(5, 109, 31),
QT_MOC_LITERAL(6, 141, 27),
QT_MOC_LITERAL(7, 169, 19),
QT_MOC_LITERAL(8, 189, 27),
QT_MOC_LITERAL(9, 217, 25)
    },
    "Image_Rechange\0on_pushButton_openDoc_clicked\0"
    "\0on_pushButton_openDocs_clicked\0"
    "on_pushButton_changeDPI_clicked\0"
    "on_pushButton_changeFBL_clicked\0"
    "on_pushButton_clear_clicked\0"
    "on_checkbox_clicked\0on_checkbox_quality_clicked\0"
    "on_checkbox_speed_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Image_Rechange[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   54,    2, 0x08 /* Private */,
       3,    0,   55,    2, 0x08 /* Private */,
       4,    0,   56,    2, 0x08 /* Private */,
       5,    0,   57,    2, 0x08 /* Private */,
       6,    0,   58,    2, 0x08 /* Private */,
       7,    0,   59,    2, 0x08 /* Private */,
       8,    0,   60,    2, 0x08 /* Private */,
       9,    0,   61,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void Image_Rechange::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Image_Rechange *_t = static_cast<Image_Rechange *>(_o);
        switch (_id) {
        case 0: _t->on_pushButton_openDoc_clicked(); break;
        case 1: _t->on_pushButton_openDocs_clicked(); break;
        case 2: _t->on_pushButton_changeDPI_clicked(); break;
        case 3: _t->on_pushButton_changeFBL_clicked(); break;
        case 4: _t->on_pushButton_clear_clicked(); break;
        case 5: _t->on_checkbox_clicked(); break;
        case 6: _t->on_checkbox_quality_clicked(); break;
        case 7: _t->on_checkbox_speed_clicked(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject Image_Rechange::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_Image_Rechange.data,
      qt_meta_data_Image_Rechange,  qt_static_metacall, 0, 0}
};


const QMetaObject *Image_Rechange::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Image_Rechange::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Image_Rechange.stringdata))
        return static_cast<void*>(const_cast< Image_Rechange*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int Image_Rechange::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 8;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
