/********************************************************************************
** Form generated from reading UI file 'image_rechange.ui'
**
** Created by: Qt User Interface Compiler version 5.3.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_IMAGE_RECHANGE_H
#define UI_IMAGE_RECHANGE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Image_RechangeClass
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QGridLayout *gridLayout_3;
    QLabel *label;
    QPushButton *pushButton_openDoc;
    QPushButton *pushButton_changeFBL;
    QPushButton *pushButton_openDocs;
    QLabel *label_2;
    QPushButton *pushButton_changeDPI;
    QCheckBox *checkBox_staydir;
    QLineEdit *lineEdit_dpi;
    QLineEdit *lineEdit_long;
    QLabel *label_3;
    QLineEdit *lineEdit_high;
    QCheckBox *checkBox_staylonghigh;
    QProgressBar *progressBar;
    QCheckBox *checkBox_quality;
    QCheckBox *checkBox_speed;
    QComboBox *comboBox;
    QTextBrowser *textBrowser;
    QPushButton *pushButton_clear;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *Image_RechangeClass)
    {
        if (Image_RechangeClass->objectName().isEmpty())
            Image_RechangeClass->setObjectName(QStringLiteral("Image_RechangeClass"));
        Image_RechangeClass->resize(1198, 578);
        centralWidget = new QWidget(Image_RechangeClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout_3 = new QGridLayout();
        gridLayout_3->setSpacing(6);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setMaximumSize(QSize(180, 16777215));
        label->setAlignment(Qt::AlignCenter);

        gridLayout_3->addWidget(label, 2, 1, 1, 1);

        pushButton_openDoc = new QPushButton(centralWidget);
        pushButton_openDoc->setObjectName(QStringLiteral("pushButton_openDoc"));
        pushButton_openDoc->setMaximumSize(QSize(180, 16777215));

        gridLayout_3->addWidget(pushButton_openDoc, 1, 0, 1, 1);

        pushButton_changeFBL = new QPushButton(centralWidget);
        pushButton_changeFBL->setObjectName(QStringLiteral("pushButton_changeFBL"));
        pushButton_changeFBL->setMaximumSize(QSize(180, 16777215));

        gridLayout_3->addWidget(pushButton_changeFBL, 3, 0, 1, 1);

        pushButton_openDocs = new QPushButton(centralWidget);
        pushButton_openDocs->setObjectName(QStringLiteral("pushButton_openDocs"));
        pushButton_openDocs->setMaximumSize(QSize(180, 16777215));

        gridLayout_3->addWidget(pushButton_openDocs, 1, 1, 1, 1);

        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setMaximumSize(QSize(180, 16777215));
        label_2->setAlignment(Qt::AlignCenter);

        gridLayout_3->addWidget(label_2, 3, 1, 1, 1);

        pushButton_changeDPI = new QPushButton(centralWidget);
        pushButton_changeDPI->setObjectName(QStringLiteral("pushButton_changeDPI"));
        pushButton_changeDPI->setMaximumSize(QSize(180, 16777215));

        gridLayout_3->addWidget(pushButton_changeDPI, 2, 0, 1, 1);

        checkBox_staydir = new QCheckBox(centralWidget);
        checkBox_staydir->setObjectName(QStringLiteral("checkBox_staydir"));
        checkBox_staydir->setMaximumSize(QSize(180, 16777215));
        checkBox_staydir->setChecked(true);

        gridLayout_3->addWidget(checkBox_staydir, 1, 2, 1, 1);

        lineEdit_dpi = new QLineEdit(centralWidget);
        lineEdit_dpi->setObjectName(QStringLiteral("lineEdit_dpi"));
        lineEdit_dpi->setMaximumSize(QSize(180, 16777215));

        gridLayout_3->addWidget(lineEdit_dpi, 2, 2, 1, 1);

        lineEdit_long = new QLineEdit(centralWidget);
        lineEdit_long->setObjectName(QStringLiteral("lineEdit_long"));
        lineEdit_long->setMaximumSize(QSize(180, 16777215));

        gridLayout_3->addWidget(lineEdit_long, 3, 2, 1, 1);

        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setContextMenuPolicy(Qt::DefaultContextMenu);
        label_3->setLayoutDirection(Qt::LeftToRight);
        label_3->setAlignment(Qt::AlignCenter);

        gridLayout_3->addWidget(label_3, 3, 3, 1, 1);

        lineEdit_high = new QLineEdit(centralWidget);
        lineEdit_high->setObjectName(QStringLiteral("lineEdit_high"));
        lineEdit_high->setMaximumSize(QSize(180, 16777215));

        gridLayout_3->addWidget(lineEdit_high, 3, 4, 1, 1);

        checkBox_staylonghigh = new QCheckBox(centralWidget);
        checkBox_staylonghigh->setObjectName(QStringLiteral("checkBox_staylonghigh"));
        checkBox_staylonghigh->setMaximumSize(QSize(180, 16777215));
        checkBox_staylonghigh->setChecked(true);

        gridLayout_3->addWidget(checkBox_staylonghigh, 3, 5, 1, 1);

        progressBar = new QProgressBar(centralWidget);
        progressBar->setObjectName(QStringLiteral("progressBar"));
        progressBar->setValue(0);

        gridLayout_3->addWidget(progressBar, 5, 0, 1, 6);

        checkBox_quality = new QCheckBox(centralWidget);
        checkBox_quality->setObjectName(QStringLiteral("checkBox_quality"));
        checkBox_quality->setMaximumSize(QSize(180, 16777215));
        checkBox_quality->setChecked(true);

        gridLayout_3->addWidget(checkBox_quality, 1, 4, 1, 1);

        checkBox_speed = new QCheckBox(centralWidget);
        checkBox_speed->setObjectName(QStringLiteral("checkBox_speed"));
        checkBox_speed->setMaximumSize(QSize(180, 16777215));

        gridLayout_3->addWidget(checkBox_speed, 1, 5, 1, 1);

        comboBox = new QComboBox(centralWidget);
        comboBox->setObjectName(QStringLiteral("comboBox"));
        comboBox->setMaximumSize(QSize(180, 16777215));

        gridLayout_3->addWidget(comboBox, 3, 6, 1, 1);

        textBrowser = new QTextBrowser(centralWidget);
        textBrowser->setObjectName(QStringLiteral("textBrowser"));

        gridLayout_3->addWidget(textBrowser, 0, 0, 1, 7);

        pushButton_clear = new QPushButton(centralWidget);
        pushButton_clear->setObjectName(QStringLiteral("pushButton_clear"));
        pushButton_clear->setMaximumSize(QSize(180, 16777215));

        gridLayout_3->addWidget(pushButton_clear, 5, 6, 1, 1);


        gridLayout->addLayout(gridLayout_3, 0, 0, 1, 1);

        Image_RechangeClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(Image_RechangeClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1198, 37));
        Image_RechangeClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(Image_RechangeClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        Image_RechangeClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(Image_RechangeClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        Image_RechangeClass->setStatusBar(statusBar);

        retranslateUi(Image_RechangeClass);

        QMetaObject::connectSlotsByName(Image_RechangeClass);
    } // setupUi

    void retranslateUi(QMainWindow *Image_RechangeClass)
    {
        Image_RechangeClass->setWindowTitle(QApplication::translate("Image_RechangeClass", "Image_Rechange", 0));
        label->setText(QApplication::translate("Image_RechangeClass", "DPI", 0));
        pushButton_openDoc->setText(QApplication::translate("Image_RechangeClass", "\346\211\223\345\274\200\346\226\207\344\273\266", 0));
        pushButton_changeFBL->setText(QApplication::translate("Image_RechangeClass", "\346\214\211\347\205\247\345\210\206\350\276\250\347\216\207\347\274\251\345\260\217", 0));
        pushButton_openDocs->setText(QApplication::translate("Image_RechangeClass", "\346\211\223\345\274\200\346\226\207\344\273\266\345\244\271", 0));
        label_2->setText(QApplication::translate("Image_RechangeClass", "\351\225\277", 0));
        pushButton_changeDPI->setText(QApplication::translate("Image_RechangeClass", "\346\214\211\347\205\247DPI\347\274\251\345\260\217", 0));
        checkBox_staydir->setText(QApplication::translate("Image_RechangeClass", "\344\277\235\346\214\201\347\233\256\345\275\225", 0));
        lineEdit_dpi->setText(QApplication::translate("Image_RechangeClass", "96", 0));
        lineEdit_long->setText(QApplication::translate("Image_RechangeClass", "1488", 0));
        label_3->setText(QApplication::translate("Image_RechangeClass", "\351\253\230", 0));
        lineEdit_high->setText(QApplication::translate("Image_RechangeClass", "2266", 0));
        checkBox_staylonghigh->setText(QApplication::translate("Image_RechangeClass", "\344\277\235\346\214\201\351\225\277\345\256\275\346\257\224", 0));
        checkBox_quality->setText(QApplication::translate("Image_RechangeClass", "\350\264\250\351\207\217\344\274\230\345\205\210", 0));
        checkBox_speed->setText(QApplication::translate("Image_RechangeClass", "\351\200\237\345\272\246\344\274\230\345\205\210", 0));
        comboBox->clear();
        comboBox->insertItems(0, QStringList()
         << QApplication::translate("Image_RechangeClass", "\344\277\235\346\214\201\351\225\277\345\272\246", 0)
         << QApplication::translate("Image_RechangeClass", "\344\277\235\346\214\201\345\256\275\345\272\246", 0)
        );
        pushButton_clear->setText(QApplication::translate("Image_RechangeClass", "\346\270\205\347\251\272", 0));
    } // retranslateUi

};

namespace Ui {
    class Image_RechangeClass: public Ui_Image_RechangeClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_IMAGE_RECHANGE_H
